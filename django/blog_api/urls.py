from django.urls import path
from .views import PostList, PostDetail, Destination, AirportList

app_name = 'blog_api'

urlpatterns = [
    path('airportuniquelist', AirportList.as_view(), name='airportuniquelist'),
    path('airportcode', Destination.as_view(), name='airportcode'),
    path('<int:pk>/', PostDetail.as_view(), name='detailcreate'),
    path('', PostList.as_view(), name='listcreate'),
]
