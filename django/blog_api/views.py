from rest_framework import generics
from blog.models import Post
from .serializers import PostSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import JsonResponse
from django.http import Http404, HttpResponse
from rest_framework import status
import csv
import json
import pandas as pd


class AirportList(APIView):

    def get(self, request, *args, **kw):

        flights = pd.read_csv ('flights.csv')

        flights_destination = flights[["destination", "destination_full_name"]]
        flights_destination = flights_destination.values.tolist()

        flights_origin = flights[["origin", "origin_full_name"]]
        flights_origin = flights_origin.values.tolist()

        all = flights_destination + flights_origin
        all = [list(x) for x in set(tuple(x) for x in all)]

        a = []
        w = {}
        for x in all:
            w = {}
            w['code'] = str(x[0])
            w['destination'] = str(x[1])
            a.append(w)


        return  JsonResponse(a, safe=False)

class Destination(APIView):

    def get(self, request, *args, **kw):

        airport_code = str(self.request.query_params.get('airport_code'))

        flights = pd.read_csv('flights.csv')
        if airport_code == "ShowAll":
            code = flights
        else:
            code = flights.loc[(flights['destination'] == airport_code) | (flights['origin'] == airport_code)]

        code = code.to_json(orient='table')
        code = json.loads(code)

        return Response(code, status=status.HTTP_200_OK)



class PostList(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class PostDetail(generics.RetrieveDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
