# ODS Full Stack Coding Assignment

## Assignment

Create a web application that allows a user to search for flights and display the results in a tabular view.

## Features

1. Allow the user to enter a station (destination or origin) to search flights. Display the results in a table.

2. Provide an auto-suggest feature for station. A user should be able to see flights based on station code or location name. For example,
for Nashville (BNA), a user should be able to select flights to/from Nashville by entering the keywords BNA or Nashville. 

3. Provide two RESTful endpoints supporting the functionality listed in steps 1 and 2.

## Datasource

A zipped CSV file of flights is available in /data/flights.csv. Each row in the CSV file represents a flight.

## Implementation

**Preferred tech stack:**
* Python
* Angular
* Docker

However, you may use other tech if you are more comfortable with something else. You can use any additional technologies/frameworks/DBs/libraries you would like to.

**To submit your solution:** 
* Clone this repo and push to a personal github repo and submit the link
* Please update the README
* Return your solution within 3 business days, unless other directions provided.
* Feel free to ask questions at any time.
* Have fun with it
