import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Select from 'react-select';
import axios from "axios";
import moment from "moment";
import {
	Badge,
} from "reactstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import {
  CardBody,
  CardTitle,
  CardHeader,
} from "reactstrap";

require('react-bootstrap-table-next/dist/react-bootstrap-table2.min.css');

const { SearchBar } = Search;

const API_DESTINATIONS = 'http://localhost:8000/api/airportuniquelist';

const API_AIRPORT_CODE = 'http://localhost:8000/api/airportcode?airport_code=';

const useStyles = makeStyles((theme) => ({
	cardMedia: {
		paddingTop: '56.25%', // 16:9
	},
	link: {
		margin: theme.spacing(1, 1.5),
	},
	cardHeader: {
		backgroundColor:
			theme.palette.type === 'light'
				? theme.palette.grey[200]
				: theme.palette.grey[700],
	},
	postTitle: {
		fontSize: '16px',
		textAlign: 'left',
	},
	postText: {
		display: 'flex',
		justifyContent: 'left',
		alignItems: 'baseline',
		fontSize: '12px',
		textAlign: 'left',
		marginBottom: theme.spacing(2),
	},
}));

const Posts = (props) => {
	const { posts } = props;
	const classes = useStyles();
	if (!posts || posts.length === 0) return <p>Can not find any posts, sorry</p>;
	return (
		<React.Fragment>
			<Container maxWidth="md" component="main">
				<h1>Latest Post</h1>
				<Grid container spacing={5} alignItems="flex-end">
					{posts.map((post) => {
						return (
							// Enterprise card is full width at sm breakpoint
							<Grid item key={post.id} xs={12} md={4}>
								<Card className={classes.card}>
									<CardMedia
										className={classes.cardMedia}
										image="https://source.unsplash.com/random"
										title="Image title"
									/>
									<CardContent className={classes.cardContent}>
										<Typography
											gutterBottom
											variant="h6"
											component="h2"
											className={classes.postTitle}
										>
											{post.title.substr(0, 50)}...
										</Typography>
										<div className={classes.postText}>
											<Typography
												component="p"
												color="textPrimary"
											></Typography>
											<Typography variant="p" color="textSecondary">
												{post.excerpt.substr(0, 60)}...
											</Typography>
										</div>
									</CardContent>
								</Card>
							</Grid>
						);
					})}
				</Grid>
			</Container>
		</React.Fragment>
	);
};

const MyExportCSV = props => {
    const handleClick = () => {
      props.onExport();
    };
    return (
      <div>
        <button className="btn btn-secondary mt-2" onClick={handleClick}>
          Export Flights
        </button>
      </div>
    );
  };

class Body extends React.Component {



	constructor(props) {
    super(props);
    this.state = {
      selectedOption: null,
			isSearchLoading: false,
			user_flight_search_api: [],
			airportcode: null,
			airportcode_table: [],
    };
  }

	fetchFlights = (airportcode) =>  {
    axios.get(`${API_AIRPORT_CODE}${airportcode}`).then(res => {
      console.log(res.data.data)
			this.setState({
				airportcode_table: res.data.data
      });
    }).catch((err) => {
      this.setState({
        airportcode_table: [],
      });
    });
  }

	fetchDestinations = () =>  {
    axios.get(`${API_DESTINATIONS}`).then(res => {
      console.log(res.data)
			this.setState({
        isSearchLoading: false,
        user_flight_search_api: res.data?.map(({ code, destination}) => {return{
          value: code,
          label:  <div className="flex-column d-flex justify-content-start align-items-start m-0 p-0">
										<div> {destination} </div>
                    <Badge style={{backgroundColor:'#D9C4EC'}}> {code} </Badge>

                  </div>,
          id: code
        }}),
      });
    }).catch((err) => {
      this.setState({
        api_bulls_loading: false,
      });
    });
  }

	componentDidMount = async () => {
    this.fetchDestinations()
  }


	handleChange = async (selectedOption) => {
    await this.setState({ selectedOption });
		await this.setState({airportcode: selectedOption.id})
		await this.fetchFlights(selectedOption.id)
    console.log(`Option selected:`, selectedOption);
  };


  render() {
    const { selectedOption } = this.state;

    return (

			<React.Fragment>
				<Container maxWidth="md" component="main">
					<h1>Search Airport</h1>
					<Select
            placeholder= "Search airport name or airport code..."
						value={selectedOption}
						onChange={this.handleChange}
						options={this.state.user_flight_search_api}
						components={{ DropdownIndicator:() => null, IndicatorSeparator:() => null }}
						noOptionsMessage={() => null}
						isLoading={this.state.isSearchLoading}
					/>


					{this.state.airportcode_table === [] ? null :

						<Card style={{marginTop: 30}}>
							<ToolkitProvider
								keyField="flight_identifier"
								data={this.state.airportcode_table}
								columns={
								[
									{
										dataField: "origin_full_name",
										text: "Origin Airport (Depart)",
										sort: false,
									},
									{
										dataField: "origin",
										text: "Origin Code (Depart)",
										sort: true,
									},
									{
										dataField: "out_gmt",
										text: "Depart Time (Depart)",
										sort: true,
										formatter: (cellContent, row) => {
											return (
												<div>
													{moment(cellContent).format('MMM D YYYY, h:mm:ss a')}
												</div>
											);
										}
									},
									{
										dataField: "destination_full_name",
										text: "Destination Airport (Arrive)",
										sort: true,
									},
									{
										dataField: "destination",
										text: "Destination Code (Arrive)",
										sort: true,
									},
									{
										dataField: "in_gmt",
										text: "Arrive Time (Arrive)",
										sort: true,
										formatter: (cellContent, row) => {
											return (
												<div>
													{moment(cellContent).format('MMM D YYYY, h:mm:ss a')}
												</div>
											);
										}
									},
								]
							}
								exportCSV
								search
							>
								{(props) => (
									<div>
										<CardHeader style={{backgroundColor:'#414a4c', display: 'flex', justifyContent:'space-between', alignItems:'center'}}>
											<div>
											<CardTitle tag="h5" style={{color: 'white'}}>Flight List {'(' + this.state.airportcode_table.length +' Results)'} </CardTitle>
											</div>


											<div style={{display: 'flex', flexDirection: 'row-reverse', float:'right'}} >
													<div style={{display: 'flex', 'justifyContent': 'center', 'alignItems': 'center'}} className="p-0 ml-1">
														<MyExportCSV {...props.csvProps} />
													</div>
													<div style={{display: 'flex', 'justifyContent': 'center', 'alignItems': 'center'}} className="p-0 mt-3">
													<SearchBar { ...props.searchProps } className="p-0 m-0"/>
													</div>
												</div>
										</CardHeader>
										<CardBody>
											<BootstrapTable
												{...props.baseProps}
												keyField="Symbol"
												bootstrap4
												bordered={false}
												rowEvents={ {
													style: { cursor: 'pointer'},
												}}
												hover={true}
												pagination={paginationFactory({
													sizePerPage: 25,
													sizePerPageList: [5, 10, 25, 50]
												})}
											/>
										</CardBody>
									</div>
								)}
							</ToolkitProvider>
						</Card>

					}





				</Container>
			</React.Fragment>



    );
  }
}

export default Body
