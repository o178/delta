# Delta

## Getting started to run the application

## build with no cache
docker-compose build --no-cache

## start the services
docker-compose up

## frontend service URL
React:            
http://localhost:3000

## backend services URL
Django:            
http://localhost:8000
DISTINCT AIRPORT
http://localhost:8000/api/airportuniquelist
SEARCH AIRPORT
http://localhost:8000/api/airportcode?airport_code=YYZ

## (troubleshooting) list the services
docker-compose ps

## (troubleshooting) if out of memory prune
docker system prune -af

# (troubleshooting) list the containers
docker ps

## stop services
docker compose stop

## down services
docker compose down

## backend
Backend is serviced by Django (Python) with libraries such as Pandas and Django Rest Framework
The Flights CSV is read by pandas (dataframes).
First we get a unique list of Airport CODE + Airport Name (combining destinations and origins).
The results are this end point:
http://localhost:8000/api/airportuniquelist
Then based on an Airport Code, we can query all the flights from and flights to the respective Airport Code:
http://localhost:8000/api/airportcode?airport_code=YYZ

##Front end
Front end is serviced by React JS with libraries such as react-select-2 and bootstrap table, and calling the backend above.
http://localhost:3000/
